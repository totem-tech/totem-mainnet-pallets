pub trait TryConvert<From, To> {
    fn try_convert(from: From) -> Option<To>;
}
