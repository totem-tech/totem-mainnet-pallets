//                              Næ§@@@ÑÉ©
//                        æ@@@@@@@@@@@@@@@@@@
//                    Ñ@@@@?.?@@@@@@@@@@@@@@@@@@@N
//                 ¶@@@@@?^%@@.=@@@@@@@@@@@@@@@@@@@@
//               N@@@@@@@?^@@@»^@@@@@@@@@@@@@@@@@@@@@@
//               @@@@@@@@?^@@@».............?@@@@@@@@@É
//              Ñ@@@@@@@@?^@@@@@@@@@@@@@@@@@@'?@@@@@@@@Ñ
//              @@@@@@@@@?^@@@»..............»@@@@@@@@@@
//              @@@@@@@@@?^@@@»^@@@@@@@@@@@@@@@@@@@@@@@@
//              @@@@@@@@@?^ë@@&.@@@@@@@@@@@@@@@@@@@@@@@@
//               @@@@@@@@?^´@@@o.%@@@@@@@@@@@@@@@@@@@@©
//                @@@@@@@?.´@@@@@ë.........*.±@@@@@@@æ
//                 @@@@@@@@?´.I@@@@@@@@@@@@@@.&@@@@@N
//                  N@@@@@@@@@@ë.*=????????=?@@@@@Ñ
//                    @@@@@@@@@@@@@@@@@@@@@@@@@@@¶
//                        É@@@@@@@@@@@@@@@@Ñ¶
//                             Næ§@@@ÑÉ©

// Copyright 2020 Chris D'Costa
// This file is part of Totem Live Accounting.
// Authors:
// - Félix Daudré-Vignier   email: felix@totemaccounting.com
// - Chris D'Costa          email: chris.dcosta@totemaccounting.com

// Totem is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Totem is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Totem.  If not, see <http://www.gnu.org/licenses/>.

//! Cross-pallet code.
//!
//! # Documentation
//!
//! Totem includes the following pallets:
//!
//! - [accounting](../pallet_accounting/index.html)
//! - [archive](../pallet_archive/index.html)
//! - [balances](../pallet_balances/index.html)
//! - [bonsai](../pallet_bonsai/index.html)
//! - [funding](../pallet_funding/index.html)
//! - [orders](../pallet_orders/index.html)
//! - [prefunding](../pallet_prefunding/index.html)
//! - [teams](../pallet_teams/index.html)
//! - [timekeeping](../pallet_timekeeping/index.html)
//! - [transfer](../pallet_transfer/index.html)

#![cfg_attr(not(feature = "std"), no_std)]

pub mod converter;
mod helpers {
    pub(crate) mod storage_map;
    pub(crate) mod try_convert;
}
pub mod types;

pub use helpers::storage_map::StorageMapExt;
pub use helpers::try_convert::TryConvert;
pub use types::traits;

#[cfg(any(test, feature = "mock"))]
pub mod mocks;
